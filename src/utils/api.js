import Axios from 'axios'

Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/'

Axios.interceptors.request.use(
  config => {
    const token = localStorage.getItem('user-token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

Axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    const originalRequest = error.config;
    const expires = localStorage.getItem('expires');
    const expiresDate = 'unix settings';
    const now = 'date settings';

    if (  error.response.status === 401 && expires && now >= expiresDate && !originalRequest._retry ) {
      originalRequest._retry = true

      // return ' Для refresh token '
    }
    else {
      return Promise.reject(error)
    }
  }
)

class Api {
  get(path, params = {}) {
    return new Promise((resolve, reject) => {
      try {
        Axios.get(path, { params }).then( resp => resolve(resp) )
      } catch (err) {
        reject( new Error(`Get methods - ${err}`) )
      }
    })
  }

  post(path, params = {}) {
    return new Promise((resolve, reject) => {
      try {
        Axios.post(path, params).then( resp => resolve(resp) )
      } catch (err) {
        reject( new Error(`Post methods - ${err}`) )
      }
    })
  }

  delete(path, params) {
    return new Promise((resolve, reject) => {
      try {
        Axios.delete(`${path}/${params}`).then( resp => resolve(resp) )
      } catch (err) {
        reject( new Error(`Delete methods - ${err}`) )
      }
    })
  }
}

export default new Api()