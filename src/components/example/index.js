import { POSTS_REQUEST } from '@/store/actions/posts'
import { mapGetters } from 'vuex'

export default {
  computed: mapGetters(['getPosts', 'getStatus']),
  mounted() {
    this.$store.dispatch(POSTS_REQUEST)
  }
}