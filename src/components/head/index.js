import { mapGetters } from 'vuex'

export default {
  data: () => ({
      list: [
        { id: 1, name: 'item 1' },
        { id: 2, name: 'item 2' },
        { id: 3, name: 'item 3' },
        { id: 4, name: 'item 4' },
        { id: 5, name: 'item 5' }
      ],
      ind: 0
  }),
  methods: {
    page(params) {
      this.ind = params
    }
  },
  computed: {
    lists() {
      return _.chunk(this.list, 3)
    },
    inds() {
      return this.ind
    }
  }
}