import { POSTS_REQUEST, POSTS_SUCCESS, POSTS_ERROR } from '../actions/posts'
import Vue from 'vue'
import Api from '@/utils/api'

const state = {
  status: '',
  list: {}
}

const getters = {
  getPosts: state => state.list,
  getStatus: state => state.status
}

const actions = {
  [POSTS_REQUEST]: ({ commit }) => {
    commit(POSTS_REQUEST)

    Api.get('posts', { _limit: 2 }).then(resp => {
      commit(POSTS_SUCCESS, resp.data)
    }).catch(() => {
      commit(POSTS_ERROR)
    })
  }
}

const mutations = {
  [POSTS_REQUEST]: state => {
    state.status = 'Loading...'
  },
  [POSTS_SUCCESS]: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'list', resp)
  },
  [POSTS_ERROR]: state => {
    state.status = 'error'
    state.list = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}