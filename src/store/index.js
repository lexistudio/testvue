import Vue from 'vue'
import posts from './modules/posts'
import Vuex from 'vuex'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== "production"

export default new Vuex.Store({
  modules: {
    posts
  },
  strict: debug
})
